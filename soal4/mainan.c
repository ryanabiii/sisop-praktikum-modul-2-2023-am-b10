#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

int main(int argc, char *argv[]) {
    int pid, status;
    char *args[4];

    // Cek jumlah argumen
    if (argc != 6) {
        fprintf(stderr, "Usage: %s <hour> <minute> <second> <asterisk> <script>\n", argv[0]);
        exit(1);
    }

    // Assign argumen ke variabel
    int hour = atoi(argv[1]);
    int minute = atoi(argv[2]);
    int second = atoi(argv[3]);
    char *asterisk = argv[4];
    char *script = argv[5];

    // Fork process
    pid = fork();

    // Cek jika terjadi error saat melakukan fork
    if (pid == -1) {
        fprintf(stderr, "Error: fork() failed\n");
        exit(1);
    }

    // Jika process adalah child
    if (pid == 0) {
        // Buat argumen baru untuk di-pass ke execv
        args[0] = script;
        args[1] = NULL;

        // Execute script dengan execv
        execv(script, args);

        // Jika terjadi error saat menjalankan script
        fprintf(stderr, "Error: execv() failed\n");
        exit(1);
    }

    // Jika process adalah parent, tunggu child selesai
    wait(&status);

    return 0;
}
