# sisop-praktikum-modul-2-2023-AM-B10

Anggota :
| Nama                         | NRP        |
|------------------------------|------------|
|Raden Pandu Anggono Rasyid    | 5025201024 |
|Ryan Abinugraha               | 5025211178 |
|Abhinaya Radiansyah Listiyanto| 5025211173 |

# Penjelasan Soal Nomor 1

Code untuk binatang.c
```
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

int main()
{
    pid_t fork1, fork2, fork3, fork4, fork5, fork6;

    // Fork pertama untuk mengunduh dan mengekstrak file binatang.zip dari Google Drive
    fork1 = fork();

    if (fork1 == 0) {
        system("wget -c 'https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq' -O binatang.zip");
        char *argv[] = {"unzip", "binatang.zip", NULL};
        execv("/bin/unzip", argv);
    }

    waitpid(fork1, NULL, 0);

    // Fork kedua untuk membuat folder HewanDarat, HewanAir, dan HewanAmphibi
    fork2 = fork();

    if (fork2 == 0) {
        char *argv[] = {"mkdir", "-p", "HewanDarat", "HewanAmphibi", "HewanAir", NULL};
        execv("/bin/mkdir", argv);
    }

    waitpid(fork2, NULL, 0);

    // Fork ketiga untuk mengelompokkan dan memindahkan file-file ke folder yang sesuai
    fork3 = fork();

    if (fork3 == 0) {
        system("ls *.jpg | shuf -n 1");
        system("mv *darat.jpg HewanDarat");
        system("mv *amphibi.jpg HewanAmphibi");
        system("mv *air.jpg HewanAir");
    }

    waitpid(fork3, NULL, 0);

    // Fork keempat untuk melakukan kompresi zip pada folder
    fork4 = fork();

    if (fork4 == 0) {
        fork5 = fork();

        if (fork5 == 0) {
            char *argv[] = {"zip","-r", "HewanDarat.zip", "HewanDarat", NULL};
            execv("/bin/zip", argv);
        }

        fork6 = fork();
                    
        if (fork6 == 0) {
            char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
            execv("/bin/zip", argv);
        }

        char *argv[] = {"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
        execv("/bin/zip", argv);
    }

    waitpid(fork4, NULL, 0);

    // Fork kelima untuk menghapus directory untuk membersihkan memory
    fork5 = fork();

    if (fork5 == 0) {
		waitpid(fork4, NULL, 0); // menunggu proses kompresi selesai
    	waitpid(fork3, NULL, 0); // menunggu proses pemindahan file selesai
    	waitpid(fork2, NULL, 0); // menunggu proses pembuatan direktori selesai

        system("rm -r HewanDarat");
        system("rm -r HewanAmphibi");
        system("rm -r HewanAir");
        system("rm -r binatang");
    }

    waitpid(fork5, NULL, 0);

    return 0;
}
```
fork pertama digunakan untuk untuk mengunduh dan mengekstrak file binatang.zip dari Google Drive menggunakan 
`system(wget -c)` untuk mengunduh file dan `execv()` untuk mengunzip filenya.

fork kedua digunakan untuk membuat folder HewanDarat, HewanAir, dan HewanAmphibi. Digunakan fungsi ` "mkdir` pada 
`char *argv[]` untuk membuat direktori / file dan `execv` untuk menjalankan perintahnya.

fork ketiga digunakan untuk mengelompokkan dan memindahkan file-file ke folder yang sesuai. Menggunakan fungsi `system()` diikuti dengan`("ls *.jpg | shuf -n 1");` untuk mengelompokkan isi file dan `("mv *darat.jpg HewanDarat");` untuk memindahkan isi file sesuai dengan gambar dengan file direktorinya.

fork keempat digunakan untuk melakukan kompresi zip pada folder. Dengan fungsi `char *argv[] = {"zip","-r", "HewanDarat.zip", "HewanDarat", NULL};` untuk melakukan kompresi zip dan fungsi `execv();` untuk menjalankan perintahnya sesuai dengan nama file direktorinya.

fork kelima digunakan untuk menghapus directory untuk membersihkan memory dengan fungsi `waitpid(fork4, NULL, 0);` untuk menunggu proses kompresi selesai, lalu fork3 untuk menunggu proses pemindahan file, dan fork2 untuk menunggu proses pembuatan direktori selesai. Selanjutnya digunakan fungsi `system("rm -r HewanDarat");` untuk menghapus serta membersihkan memory.

```
# Penjelasan Soal Nomor 2

Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

    a). Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat  sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].
    b). Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
    c). Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).
    d). Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
    e). Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

#Jawab 2(a)

dalam membuat folder yang diminta kami menggunakan fork serta exec untuk memanggil command mkdir, dan fungsi strftime karena bentuk dibutuhkan folder dengan timestamp[YYYY-mm-dd_HH:ii:ss]. lalu daemon dipakai dengan selang waktu 30 dtk dengan sleep 30 dtk.

```
pid_t cid, cid2, cid3, cid4;
time_t t1 = time(NULL);
struct tm* p1 = localtime(&t1);
strftime(curtime, 30, "%Y-%m-%d_%H:%M:%S", p1);

cid = fork();
if(cid < 0) exit(0);
if(cid == 0)
{
    char *ag[] = {"mkdir", curtime, NULL};
    execv("/bin/mkdir", ag);
}
sleep(30);

```
#Jawab 2(b)

untuk mendownload gambar, digunakan fungsi wget dan chdir(curtime) agar bisa masuk ke direktrori, lalu dilakukan iterasi selama 15 kali seperti yang diminta

```
chdir(curtime);
for(i = 0; i < 15; i++)
{
    time_t t2 = time(NULL);
    struct tm* p2 = localtime(&t2);
    strftime(curtime2, 30, "%Y-%m-%d_%H:%M:%S", p2);
    sprintf(link, "https://picsum.photos/%ld", (t2 % 1000) + 50);

    cid3 = fork();
    if(cid3 < 0) exit(0);
    if(cid3 == 0)
    {
        char *ag[] = {"wget", link, "-O", curtime2, "-o", "/dev/null", NULL};
        execv("/usr/bin/wget", ag);
    }
    sleep(5);
}
```

#Jawab 2(c)

karena ingin dirapikan menjadi zip dan mendelete folder sebelumnya, maka kita copy dulu foldernya dengan fungsi strcpy kemudian strcat untuk menambahkan ekstensi .zip. kemudian hapus folder sebelumnya dengan command rm -r

```
while(wait(&stat2) > 0);
chdir("..");
strcpy(curtime3, curtime);
strcat(curtime3, ".zip");

cid4 = fork();
if(cid4 < 0) exit(0);
if(cid4 == 0)
{
    char *ag[] = {"zip", "-r", curtime3, curtime, NULL};
    execv("/usr/bin/zip", ag);
}
while(wait(&stat3) > 0);

char *ag[] = {"rm", "-r", curtime, NULL};
execv("/bin/rm", ag);
```

#Jawab 2(d)

killer dibuat dalam bentuk bash. command fopen digunakan untuk membuat file baru yang akan dituliskan, dengan command w, dan command fprintf untuk menuliskan source code killer. lalu command chmod u+x kill.sh untuk user dapat mengeksekusi file kill.sh tersebut.

```
FILE* kill;
kill = fopen("kill.sh", "w");
fprintf(kill, "#!/bin/bash\nkill %d\nkill %d\necho \'Killed program.\'\nrm \"$0\"", getpid() + 2, getpid() + 3);
fclose(kill);
pid_t cid;
cid = fork();
if(cid < 0) exit(0);
if(cid = 0)
{
    char *ag[] = {"chmod", "u+x", "kill.sh", NULL};
    execv("/bin/chmod", ag);
}
while(wait(&stat) > 0);
```

#Jawab 2(e)

untuk membuat mode a dan b, digunakan parameter arg(banyak argumen), dan array argpass untuk argumen yang dipassing. karena di mode_a program utama akan langsung menghentikan semua operasinya, maka dibuat kondisi agar yaitu while(1) agar ketika yang dipilih mode_a maka akan hangup dengan fungsi prctl

```
int main(int arg, char* argpass[])
{
    if(arg != 2 || (argpass[1][1] != 'a' && argpass[1][1] != 'b')) 
    {
        printf("Pilih mode -a atau -b\n");
        exit(0);
    }
    ...
    while(1)
    {
        ...
        if(argpass[1][1] == 'a') prctl(PR_SET_PDEATHSIG, SIGHUP);
        ...
    }
    ...
}
```
# Penjelasan Soal Nomor 3
Soal 3 terdapat seorang pelatih yang meminta bantuan kepada kita sebagai programer untuk membantu mengumpulkan pemain MU dengan ketentuan tidak boleh menggunakan fungsi system() tetapi menggunakan exec() dan fork()

Pada bagian pertama kita diminta untuk mendownload sebuah file driver berupa link yang berisi data pemain sepakbola berbentuk zip, jadi digunakan fungsi "wget" (Link yang ingin diakses) dan "-O" berfungsi untuk memberikan nama file yang ingin di download dan mengekstrak zip yang sudah didowload menjadi folder biasa, dengan cara fungsi "unzip" untuk mengekstrak file zip kemudia "-d" untuk meletakkan hasil ekstaknya akan dimana dan "." directory saat ini
```
    // Download file zip
    pid11 = fork();
    if (pid11 == 0) {
        char *argv[] = {"wget", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", "-O", "player.zip", NULL};
        execv("/usr/bin/wget", argv);
    } else {
        waitpid(pid11, &status, 0);
        if (WEXITSTATUS(status) != 0) {
            return 1;
        }
    }

    // Extract file zip
    pid21 = fork();
    if (pid21 == 0) {
        char *un[] = {"unzip", "-q", "player.zip", "-d", ".", NULL};
        execv("/usr/bin/unzip", un);
    } else {
        waitpid(pid21, &status, 0);
        if (WEXITSTATUS(status) != 0) {
            return 1;
        }
    }
```

Pada Bagian Kedua diminta untuk memfilter nama pemain selain dari MU akan terhapus buka foldernya terlebih dahulu dengan fungsi "open" setelah itu pindah ke bagian parentnya "rm" remove "!" "ManUTD" selain pemain MU
```
    // Cari nama pemain Manchester United
    pid31 = fork();
    if (pid31 == 0) {
    	char *path = "players";
        char *op[] = {"open", path, NULL};
        execv("/usr/bin/find", op);
    } else {
        waitpid(pid31, &status, 0);
        char *fl[] = {"find", ".", "-type", "f", "!", "-name", "*ManUtd*", "-exec", "rm", "{}", "+", NULL};
        execv("/usr/bin/find", fl);
        if (WEXITSTATUS(status) != 0) {
            return 1;
        }
    }
}
```

Pada bagian Ketiga diminta untuk memasukkan pemain ke dalam folder sesuai dengan posisinya, pertama-tama membuat folder dulu dengan nama posisi penyerang, kiper, gelandang, dan bek dengan fugnsi "mkdir", setelah membuat folder memindahkan pemain MU sesuai dengan posisinya dengan cara "find" "-f" "name" "posisi masing2" dan memindahkannya dengan fungsi "mv"
```
    // Membuat folder posisi
    pid41 = fork();
    if (pid41 == 0) {
        char *args[] = {"mkdir", "-p", "players/Kiper", NULL};
        execv("/usr/bin/mkdir", args);
    } else {
        pid51 = fork();
        if (pid51 == 0) {
            char *args[] = {"mkdir", "-p", "players/Bek", NULL};
        	execv("/usr/bin/mkdir", args);
        } else {
            pid_t pid61 = fork();
            if (pid61 == 0) {
                char *args[] = {"mkdir", "-p", "players/Penyerang", NULL};
        		execv("/usr/bin/mkdir", args);
            } else {
            	pid_t pid71 = fork();
            	if (pid71 == 0) {
            	    char *args[] = {"mkdir", "-p", "players/Gelandang", NULL};
					execv("/usr/bin/mkdir", args);
				} else {
	                waitpid(pid41, &status, 0);
	                waitpid(pid51, &status, 0);
	                waitpid(pid61, &status, 0);
	                waitpid(pid71, &status, 0);
	                if (WEXITSTATUS(status) != 0) {
	                    return 1;
	                }
				}
            }
        }
    }
    
    // Move players
    pid_t pid81, pid91, pid101, pid111;
    
    pid81 = fork();
    if (pid81 == 0) {
        char *args[] = {"find", ".", "-type", "f", "-name", "*Kiper*", "-exec", "mv", "{}", "players/Kiper/", ";", NULL};
        execv("/usr/bin/find", args);
    } else {
        pid91 = fork();
        if (pid91 == 0) {
            char *args[] = {"find", ".", "-type", "f", "-name", "*Bek*", "-exec", "mv", "{}", "players/Bek/", ";", NULL};
        	execv("/usr/bin/find", args);
        } else {
            pid101 = fork();
            if (pid101 == 0) {
                char *args[] = {"find", ".", "-type", "f", "-name", "*Penyerang*", "-exec", "mv", "{}", "players/Penyerang/", ";", NULL};
        		execv("/usr/bin/find", args);
            } else {
            	pid111 = fork();
            	if (pid111 == 0) {
            		char *args[] = {"find", ".", "-type", "f", "-name", "*Gelandang*", "-exec", "mv", "{}", "players/Gelandang/", ";", NULL};
					execv("/usr/bin/find", args);
				} else {
	                waitpid(pid81, &status, 0);
	                waitpid(pid91, &status, 0);
	                waitpid(pid101, &status, 0);
	                waitpid(pid111, &status, 0);
	                if (WEXITSTATUS(status) != 0) {
	                    return 1;
	                }
				}
            }
        }
    }
```

# Penjelasan Soal Nomor 4
Soal 4 diminta untuk membuat sebuah time managament untuk script bash fungsinya sama seperti corntab tetapi disini menggunakan bahasa c dan tidak menggunakan fungsi system() dan mengeluarkan pesan error, disini menggunakan fungsi exec() dan fork() pertama membuat sebuah argumen yang dirubah menjadi variable : "hour" "minute" "second" "asterisk" "script" kemudian bagian child akan memproses script bash yang ingin dijalankan. contoh menjalankan program = ./program \* 10 03 0 /home/vagrant/script.sh
```
// Assign argumen ke variabel
    int hour = atoi(argv[1]);
    int minute = atoi(argv[2]);
    int second = atoi(argv[3]);
    char *asterisk = argv[4];
    char *script = argv[5];

// Jika process adalah child
    if (pid == 0) {
        // Buat argumen baru untuk di-pass ke execv
        args[0] = script;
        args[1] = NULL;

        // Execute script dengan execv
        execv(script, args);

        // Jika terjadi error saat menjalankan script
        fprintf(stderr, "Error: execv() failed\n");
        exit(1);
    }
```
