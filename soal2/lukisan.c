#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <signal.h>

// A
pid_t cid, cid2, cid3, cid4;
time_t t1 = time(NULL);
struct tm* p1 = localtime(&t1);
strftime(curtime, 30, "%Y-%m-%d_%H:%M:%S", p1);

cid = fork();
if(cid < 0) exit(0);
if(cid == 0)
{
    char *ag[] = {"mkdir", curtime, NULL};
    execv("/bin/mkdir", ag);
}
sleep(30);

// B
chdir(curtime);
for(i = 0; i < 15; i++)
{
    time_t t2 = time(NULL);
    struct tm* p2 = localtime(&t2);
    strftime(curtime2, 30, "%Y-%m-%d_%H:%M:%S", p2);
    sprintf(link, "https://picsum.photos/%ld", (t2 % 1000) + 50);

    cid3 = fork();
    if(cid3 < 0) exit(0);
    if(cid3 == 0)
    {
        char *ag[] = {"wget", link, "-O", curtime2, "-o", "/dev/null", NULL};
        execv("/usr/bin/wget", ag);
    }
    sleep(5);
}

// C
while(wait(&stat2) > 0);
chdir("..");
strcpy(curtime3, curtime);
strcat(curtime3, ".zip");

cid4 = fork();
if(cid4 < 0) exit(0);
if(cid4 == 0)
{
    char *ag[] = {"zip", "-r", curtime3, curtime, NULL};
    execv("/usr/bin/zip", ag);
}
while(wait(&stat3) > 0);

char *ag[] = {"rm", "-r", curtime, NULL};
execv("/bin/rm", ag);

// D
FILE* kill;
kill = fopen("kill.sh", "w");
fprintf(kill, "#!/bin/bash\nkill %d\nkill %d\necho \'Killed program.\'\nrm \"$0\"", getpid() + 2, getpid() + 3);
fclose(kill);
pid_t cid;
cid = fork();
if(cid < 0) exit(0);
if(cid = 0)
{
    char *ag[] = {"chmod", "u+x", "kill.sh", NULL};
    execv("/bin/chmod", ag);
}
while(wait(&stat) > 0);

// E
int main(int arg, char* argpass[])
{
    if(arg != 2 || (argpass[1][1] != 'a' && argpass[1][1] != 'b')) 
    {
        printf("Pilih mode -a atau -b\n");
        exit(0);
    }
    ...
    while(1)
    {
        ...
        if(argpass[1][1] == 'a') prctl(PR_SET_PDEATHSIG, SIGHUP);
        ...
    }
    ...
}
