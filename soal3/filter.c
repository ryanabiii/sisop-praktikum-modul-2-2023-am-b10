#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

int main() {
    pid_t pid11, pid21, pid31, pid41, pid51;
    int status;

    // Download file zip
    pid11 = fork();
    if (pid11 == 0) {
        char *argv[] = {"wget", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", "-O", "player.zip", NULL};
        execv("/usr/bin/wget", argv);
    } else {
        waitpid(pid11, &status, 0);
        if (WEXITSTATUS(status) != 0) {
            return 1;
        }
    }

    // Extract file zip
    pid21 = fork();
    if (pid21 == 0) {
        char *un[] = {"unzip", "-q", "player.zip", "-d", ".", NULL};
        execv("/usr/bin/unzip", un);
    } else {
        waitpid(pid21, &status, 0);
        if (WEXITSTATUS(status) != 0) {
            return 1;
        }
    }


    // Membuat folder posisi
    pid41 = fork();
    if (pid41 == 0) {
        char *args[] = {"mkdir", "-p", "players/Kiper", NULL};
        execv("/usr/bin/mkdir", args);
    } else {
        pid51 = fork();
        if (pid51 == 0) {
            char *args[] = {"mkdir", "-p", "players/Bek", NULL};
        	execv("/usr/bin/mkdir", args);
        } else {
            pid_t pid61 = fork();
            if (pid61 == 0) {
                char *args[] = {"mkdir", "-p", "players/Penyerang", NULL};
        		execv("/usr/bin/mkdir", args);
            } else {
            	pid_t pid71 = fork();
            	if (pid71 == 0) {
            	    char *args[] = {"mkdir", "-p", "players/Gelandang", NULL};
					execv("/usr/bin/mkdir", args);
				} else {
	                waitpid(pid41, &status, 0);
	                waitpid(pid51, &status, 0);
	                waitpid(pid61, &status, 0);
	                waitpid(pid71, &status, 0);
	                if (WEXITSTATUS(status) != 0) {
	                    return 1;
	                }
				}
            }
        }
    }
    
    // Move players
    pid_t pid81, pid91, pid101, pid111;
    
    pid81 = fork();
    if (pid81 == 0) {
        char *args[] = {"find", ".", "-type", "f", "-name", "*Kiper*", "-exec", "mv", "{}", "players/Kiper/", ";", NULL};
        execv("/usr/bin/find", args);
    } else {
        pid91 = fork();
        if (pid91 == 0) {
            char *args[] = {"find", ".", "-type", "f", "-name", "*Bek*", "-exec", "mv", "{}", "players/Bek/", ";", NULL};
        	execv("/usr/bin/find", args);
        } else {
            pid101 = fork();
            if (pid101 == 0) {
                char *args[] = {"find", ".", "-type", "f", "-name", "*Penyerang*", "-exec", "mv", "{}", "players/Penyerang/", ";", NULL};
        		execv("/usr/bin/find", args);
            } else {
            	pid111 = fork();
            	if (pid111 == 0) {
            		char *args[] = {"find", ".", "-type", "f", "-name", "*Gelandang*", "-exec", "mv", "{}", "players/Gelandang/", ";", NULL};
					execv("/usr/bin/find", args);
				} else {
	                waitpid(pid81, &status, 0);
	                waitpid(pid91, &status, 0);
	                waitpid(pid101, &status, 0);
	                waitpid(pid111, &status, 0);
	                if (WEXITSTATUS(status) != 0) {
	                    return 1;
	                }
				}
            }
        }
    }
    
    // Cari nama pemain Manchester United
    pid31 = fork();
    if (pid31 == 0) {
    	char *path = "players";
        char *op[] = {"open", path, NULL};
        execv("/usr/bin/find", op);
    } else {
        waitpid(pid31, &status, 0);
        char *fl[] = {"find", ".", "-type", "f", "!", "-name", "*ManUtd*", "-exec", "rm", "{}", "+", NULL};
        execv("/usr/bin/find", fl);
        if (WEXITSTATUS(status) != 0) {
            return 1;
        }
    }
}
