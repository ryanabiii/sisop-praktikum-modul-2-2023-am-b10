#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

int main()
{
    pid_t fork1, fork2, fork3, fork4, fork5, fork6;

    // Fork pertama untuk mengunduh dan mengekstrak file binatang.zip dari Google Drive
    fork1 = fork();

    if (fork1 == 0) {
        system("wget -c 'https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq' -O binatang.zip");
        char *argv[] = {"unzip", "binatang.zip", NULL};
        execv("/bin/unzip", argv);
    }

    waitpid(fork1, NULL, 0);

    // Fork kedua untuk membuat folder HewanDarat, HewanAir, dan HewanAmphibi
    fork2 = fork();

    if (fork2 == 0) {
        char *argv[] = {"mkdir", "-p", "HewanDarat", "HewanAmphibi", "HewanAir", NULL};
        execv("/bin/mkdir", argv);
    }

    waitpid(fork2, NULL, 0);

    // Fork ketiga untuk mengelompokkan dan memindahkan file-file ke folder yang sesuai
    fork3 = fork();

    if (fork3 == 0) {
        system("ls *.jpg | shuf -n 1");
        system("mv *darat.jpg HewanDarat");
        system("mv *amphibi.jpg HewanAmphibi");
        system("mv *air.jpg HewanAir");
    }

    waitpid(fork3, NULL, 0);

    // Fork keempat untuk melakukan kompresi zip pada folder
    fork4 = fork();

    if (fork4 == 0) {
        fork5 = fork();

        if (fork5 == 0) {
            char *argv[] = {"zip","-r", "HewanDarat.zip", "HewanDarat", NULL};
            execv("/bin/zip", argv);
        }

        fork6 = fork();
                    
        if (fork6 == 0) {
            char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
            execv("/bin/zip", argv);
        }

        char *argv[] = {"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
        execv("/bin/zip", argv);
    }

    waitpid(fork4, NULL, 0);

    // Fork kelima untuk menghapus directory untuk membersihkan memory
    fork5 = fork();

    if (fork5 == 0) {
		waitpid(fork4, NULL, 0); // menunggu proses kompresi selesai
    	waitpid(fork3, NULL, 0); // menunggu proses pemindahan file selesai
    	waitpid(fork2, NULL, 0); // menunggu proses pembuatan direktori selesai

        system("rm -r HewanDarat");
        system("rm -r HewanAmphibi");
        system("rm -r HewanAir");
        system("rm -r binatang");
    }

    waitpid(fork5, NULL, 0);

    return 0;
}
